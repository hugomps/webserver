package org.academiadecodigo.bootcamp;

import java.io.*;
import java.net.*;

public class WebServer {

    private int port = 4020;
    private BufferedReader br;
    private BufferedReader brw;
    private String content = "";
    private String fileType;
    private String header = "";
    private DataOutputStream dos;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private String resourcePath;


    public static void main(String[] args) throws IOException {

        try {

            WebServer webServer = new WebServer();

            while(true) {
                webServer.createSocket();
                webServer.getInfoBrowser();


                if (webServer.fileCanRead()) {
                    webServer.printStuff();
                } else {
                    webServer.page404();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void createSocket() throws IOException {
        serverSocket = new ServerSocket(port);
        System.out.println(serverSocket.isBound() ? "TRUE" : "FALSE");
        clientSocket = serverSocket.accept();
        System.out.println(clientSocket.isBound() ? "O client socket foi aceite" : "o client socket não foi aceite");

    }

    public void getInfoBrowser() throws IOException {


        try {

            //buffer para o input
            br = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            //Transformar numa array o endereço e guardar numa string a informacao a seguir à /

            String browser = br.readLine();
            System.out.println(browser);
            String[] rPath = browser.split("[ /]+");
            resourcePath = rPath[1];
            System.out.println(resourcePath);

            //Transformar numa array o endereço e guardar numa string a informacao a seguir à "." para saber o tipo
            String[] typeOfFile = resourcePath.split("\\.");
            fileType = typeOfFile[1];
            System.out.println(fileType);
        } catch (IOException ex) {
            System.out.println("receive error");
        }
    }




    public void printStuff () throws IOException {
        dos = new DataOutputStream(clientSocket.getOutputStream());

        if (fileType.equals("html")) {
            brw = new BufferedReader(new FileReader(resourcePath));

            while (brw.ready()) {
                content += brw.readLine();
            }

            header = "HTTP/1.0 200 Document Follows\r\n" +
                    "Content-Type: text/html; charset=UTF-8\r\n" +
                    "Content-Length: " + content.length() + " \r\n" +
                    "\r\n";

            dos.writeBytes(header + content);
        }
        if (fileType.equals("jpg")) {
            FileInputStream inputStream = new FileInputStream(resourcePath);
            header = "HTTP/1.0 200 Document Follows\r\n" +
                    "Content-Type: image/jpg\r\n" +
                    "Content-Length: <file_byte_size> \r\n" +
                    "\r\n";
            dos.writeBytes(header);
            int num = 0;
            byte[] buffer = new byte[1024];
            while ((num = inputStream.read(buffer)) != -1) {
                System.out.println(num);
                dos.write(buffer,0,num);
            }
        }
        clientSocket.close();
        serverSocket.close();

    }


    public boolean fileCanRead() {
        File file = new File(resourcePath);
        System.out.println(file.canRead());
        return file.canRead();
    }


    public void page404 () throws IOException {
        dos = new DataOutputStream(clientSocket.getOutputStream());
        brw = new BufferedReader(new FileReader("page404.html"));

        while (brw.ready()) {
            content += brw.readLine();
        }

        header = "HTTP/1.0 404 Document Follows\r\n" +
                "Content-Type: text/html; charset=UTF-8\r\n" +
                "Content-Length: " + content.length() + " \r\n" +
                "\r\n";

        dos.writeBytes(header + content);

        clientSocket.close();
        serverSocket.close();
    }
}
